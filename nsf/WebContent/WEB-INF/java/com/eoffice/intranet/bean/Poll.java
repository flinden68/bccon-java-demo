package com.eoffice.intranet.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

import lotus.domino.Document;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewEntryCollection;

import com.eoffice.basic.configuration.Configuration;
import com.eoffice.intranet.charts.BarChart;
import com.eoffice.intranet.charts.Chart;
import com.eoffice.intranet.charts.PieChart;
import com.eoffice.utils.JSFUtil;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class Poll implements Serializable {

	private static final long serialVersionUID = 1L;
	private DominoDocument pollDocument;
	private String pollID;
	private Chart chart;
	
	/**********************
	 * Constructor        *
	 **********************/
	public Poll(){
		
	}
	/**********************
	 * Methods            *
	 **********************/
	public void init(String pollID){
		this.getPage(pollID);
		this.setPollID(pollID);
		this.loadChartData(pollID);
	}
	
	private void getPage(String pollID){
		View vwPolls = null;
		Document docPoll = null;
		try{
			if(pollID!=null){
				vwPolls = JSFUtil.getCurrentDatabase().getView("contentByPollID");
				docPoll = vwPolls.getDocumentByKey(pollID, true);
			}
			if(docPoll!=null){
				this.setPollDocument(DominoDocument.wrap(docPoll.getParentDatabase().getFilePath(),docPoll,null,null,false,null,null));
				docPoll.recycle();
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(docPoll, vwPolls);
		}
	}
	
	@SuppressWarnings("unchecked")	
	
	private void loadChartData(String pollID){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}

			if(this.getFieldValue("chartType").equals("bar")){
				chart = new BarChart();
			}else if(this.getFieldValue("chartType").equals("pie")){
				chart = new PieChart();
			}
			
			chart.loadData(pollID, (Vector<String>) this.getFieldValue("answers"));
		}catch(Exception e){
				Configuration.getOpenLogItem().logError(e);
		}finally{

		}
	}
	
	public Object getFieldValue(String field){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			if(pollDocument.hasItem(field)){
				return pollDocument.getValue(field);
			}

		}catch(Exception e){
			return "";
		}
		return null;
	}
	
	public boolean isTrue(String field){
		if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
			pollDocument.restoreWrappedDocument(); 
		}
		return Boolean.parseBoolean((String) pollDocument.getValue(field));
	}
	
	public boolean hasItem(String field){		
		try{
		if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
			pollDocument.restoreWrappedDocument(); 
		}
		return pollDocument.hasItem(field);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{

		}
		return false;
	}
	
	public boolean hasDocument(String viewName, String documentid, boolean useUsername){
		
		View vw = null;
		String key = null;
		try{
			vw = JSFUtil.getCurrentDatabase().getView(viewName);

			if(vw!=null){
				if(useUsername){
					String userName=JSFUtil.getCurrentSession().getEffectiveUserName();
					key = new String(userName+"##"+documentid);
				}else{
					key = new String(documentid);
				}
				Document doc = vw.getDocumentByKey(key, true);
				if(doc!=null){
					doc.recycle();
					return true;
				}
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(vw);
		}
		return false;
	}
	
	public void update(String newTitle, String newContentID, String newContentType){
		try {
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			
			pollDocument.setValue("widgetTitle",newTitle);
			pollDocument.setValue("contentID",newContentID);
			pollDocument.setValue("contentType",newContentType);

			pollDocument.save();

		} catch (Exception e) {
			Configuration.getOpenLogItem().logError(e);
		}
	}
	
	
	
	public int getPollAnswerTotal(String viewName, String key){
		View vw = null;
		try{
			vw = JSFUtil.getCurrentDatabase().getView(viewName);

			if(vw!=null){
				return vw.createViewNavFromCategory(key).getCount();
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(vw);
		}
		return 0;
	}
	
	public Double getColumnTotal(String viewName, int column, String key){
		View vw = null;
		ViewEntryCollection nav = null;
		ViewEntry entry = null;
		ViewEntry entryNext = null;
		Double total = new Double(0);
		try{
			vw = JSFUtil.getCurrentDatabase().getView(viewName);

			if(vw!=null){
				nav = vw.getAllEntriesByKey(key, true);
				if(nav.getCount()>0){
					entry = nav.getFirstEntry();
					
					while(entry!=null){
						entryNext = nav.getNextEntry(entry);
						if(entry.isTotal()){
							total = (Double) entry.getColumnValues().elementAt(column);
							break;
						}
						total += (Double) entry.getColumnValues().elementAt(column);
						entry.recycle();
						entry = entryNext;
					}
				}
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(entryNext, entry, nav, vw);
		}
		return total;
	}
	
	public boolean isTodayInBetween(String fieldStart, String fieldEnd){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			
			if(pollDocument.hasItem(fieldStart)|| pollDocument.hasItem(fieldEnd)){
				Date dateStart = pollDocument.getItemValueDateTime(fieldStart).toJavaDate();
				Date dateEnd = pollDocument.getItemValueDateTime(fieldEnd).toJavaDate();
				return JSFUtil.isTodayInBetween(dateStart, dateEnd);
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;	
	}
	
	public boolean isInBetween(String fieldCheck, String fieldStart, String fieldEnd){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			if(pollDocument.hasItem(fieldCheck)||pollDocument.hasItem(fieldStart)|| pollDocument.hasItem(fieldEnd)){
				Date dateCheck = pollDocument.getItemValueDateTime(fieldCheck).toJavaDate();
				Date dateStart = pollDocument.getItemValueDateTime(fieldStart).toJavaDate();
				Date dateEnd = pollDocument.getItemValueDateTime(fieldEnd).toJavaDate();
				return JSFUtil.isDateInBetween(dateCheck, dateStart, dateEnd);
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;	
	}
	
	public boolean isAfterToday(String fieldDate){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			if(pollDocument.hasItem(fieldDate)){
				Date dateCheck = pollDocument.getItemValueDateTime(fieldDate).toJavaDate();
				return JSFUtil.isDateAfterToday(dateCheck);
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;	
	}
	
	public boolean isBefore(String fieldDate, String fieldCompare){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			if(pollDocument.hasItem(fieldDate)|| pollDocument.hasItem(fieldCompare)){
				Date dateDate = pollDocument.getItemValueDateTime(fieldDate).toJavaDate();
				Date dateCompare = pollDocument.getItemValueDateTime(fieldCompare).toJavaDate();
				return JSFUtil.isDateBefore(dateDate, dateCompare);
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;	
	}
	
	public boolean isBeforeToday(String fieldDate){
		try{
			if(JSFUtil.isRecycled(pollDocument.getDocument())) { 
				pollDocument.restoreWrappedDocument(); 
			}
			if(pollDocument.hasItem(fieldDate)){
				Date dateDate = pollDocument.getItemValueDateTime(fieldDate).toJavaDate();
				return JSFUtil.isDateBeforeToday(dateDate);
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;	
	}
	
	/**********************
	 * Getters and setter *
	 **********************/
	public void setPollDocument(DominoDocument pollDocument) {
		this.pollDocument = pollDocument;
	}
	public DominoDocument getPollDocument() {
		return pollDocument;
	}
	public void setPollID(String pollID) {
		this.pollID = pollID;
	}
	public String getPollID() {
		return pollID;
	}
	public Chart getChart() {
		return chart;
	}
	public void setChart(Chart chart) {
		this.chart = chart;
	}
}
