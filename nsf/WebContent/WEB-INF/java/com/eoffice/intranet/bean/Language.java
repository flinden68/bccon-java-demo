package com.eoffice.intranet.bean;

import static com.ibm.xsp.extlib.util.ExtLibUtil.resolveVariable;
import java.io.Serializable;
import java.util.ResourceBundle;

import com.eoffice.basic.configuration.Configuration;
import com.eoffice.utils.JSFUtil;

public class Language implements Serializable {
	private static final String DEFAULT_LANGUAGE = "en_basic";
	private static final long serialVersionUID = 1L;
	//private String bundleName = "en_basic";
	private static String bundleVar = "en_basic";
	
	public Language(){
		this.loadLanguage();
	}
	
	public Object getGlobalObject(String obj) throws Exception {
		return resolveVariable(JSFUtil.getFacesContext(),obj);
	}
	
	public void loadLanguage(){
		this.setBundleVar(DEFAULT_LANGUAGE);
	}
	
	public static String getLanguageString(String key) {
		 try {
			 // There has to be bundle defined with var name <bundlevar>
			 ResourceBundle bundle;
			 bundle = JSFUtil.getXSPContext().bundle(bundleVar);
			 if (bundle != null) {
				 return bundle.getString(key);
			 }
		 } catch (Exception e) {
			 Configuration.getOpenLogItem().logError(e);
		 }
		 	return "";
		 }

	public void setBundleVar(String bundleVar) {
		Language.bundleVar = bundleVar;
	}

	public String getBundleVar() {
		return bundleVar;
	}
}
