package com.eoffice.intranet.bean;

import java.io.Serializable;

public class DemoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String property1;
	private String property2;
	
	public DemoBean(){
		
	}

	public void setProperty1(String property1) {
		this.property1 = property1;
	}

	public String getProperty1() {
		return property1;
	}

	public void setProperty2(String property2) {
		this.property2 = property2;
	}

	public String getProperty2() {
		return property2;
	}
}
