package com.eoffice.intranet.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;

import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

import com.eoffice.basic.configuration.Configuration;
import com.eoffice.utils.JSFUtil;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class ContentController implements Serializable {

	private static final long serialVersionUID = 1L;
	private String contentType;
	private String status;
	private Vector<String> answers;
	private DominoDocument contentDocument;
	private Date date;
	
	/**********************
	 * Constructor        *
	 **********************/
	public ContentController(){
		
	}
	
	public void init() throws NotesException{
	    calculateDate();
	}
	
	//TODO: add private Date date;
	public void calculateDate(){
	    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	        String dateInString = JSFUtil.getQueryStringParameter("date");
	        Date date;
	 
	        try {
                	 if(!"".equals(dateInString)){
            	     Calendar cal = Calendar.getInstance();
                         cal.setTime(new Date());
            	     String time = new String(cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND));
            	     date = formatter.parse(dateInString+" "+time);
                	          
                	 }else{
                	     date = new Date();
                	 }
	                setDate(date);
	                
	                //TODO
	                //change the calculateTimestamp and calculateWeekstamp to use the date property.
	 
	        } catch (ParseException e) {
	                e.printStackTrace();
	        }
	}
	
	public String calculateURLForNewDate(String fieldInput){
	    String inputDateValue = (String) JSFUtil.getSubmittedValue(fieldInput);
	    
	    String[] values = inputDateValue.split("-");
	    String dateValue = values[2]+"-"+values[1]+"-"+values[0];

	    //TODO new method in JSFUtil
	    /*public static String getRequestURI(){
	            return ((HttpServletRequest) getRequest()).getRequestURI();
	     }*/
	    String uri = JSFUtil.getRequestURI();
	    
	    return uri+"?date="+dateValue;   
	}
	
	public void getPage(String contentID){
		View vw = null;
		try{ 
			vw = JSFUtil.getCurrentDatabase().getView("contentByID");
			Document doc = vw.getDocumentByKey(contentID, true);
			if(doc!=null){
				this.setContentDocument(DominoDocument.wrap(doc.getParentDatabase().getFilePath(),doc,null,null,false,null,null));
				doc.recycle();
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(vw);
		}
	}
	
	public void addAnswer(){
		try{
			UIComponent addComp = JSFUtil.findComponent("addAnswerField");
			UIComponent inputComp = JSFUtil.findComponent("inputAnswer");
			String valueToAdd = (String) JSFUtil.getSubmittedValue("addAnswerField");
			
			if(answers==null){
				answers = new Vector<String>();
			}
			
			if(!valueToAdd.isEmpty()){
				
				if(!answers.contains(valueToAdd)){
					answers.add(valueToAdd);
				}

				((UIInput) inputComp).setValue(answers);
				//make input field blank
				((UIInput) addComp).setValue("");
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}
	}
	
	public void removeAnswer(String removeValue){
		try{
			UIComponent inputComp = JSFUtil.findComponent("inputAnswer");
			
			if(answers.contains(removeValue)){
				answers.remove(removeValue);
			}
			
			((UIInput) inputComp).setValue(answers);
			
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}
	}
	
	public int generateRandomNumber(int size){
		Random randomGenerator = new Random();
		return randomGenerator.nextInt(size);
	}
	
	public void calculateWeekNumber() {
	    Date date = new Date();
	    
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    int weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
	    System.out.println(weekNumber);
	}
	
    public void setDateToCertainTime(){
	    //Date shiftDate = new Date("07-04-2014 06:00:00");
	    
	    //SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy");
	    Date date;
        try {           
            //System.out.println(shiftDate);
            date = new Date();
            System.out.println(date);
            
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            
            /*Calendar calShift = Calendar.getInstance();
            calShift.setTime(shiftDate);*/
            int hour = 6;//calShift.get(Calendar.HOUR);
            System.out.println(hour);
            
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), hour, 0,0);
            date = cal.getTime();
            System.out.println(date);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	    
	}
	
	/**********************
	 * Getters and setter *
	 **********************/
	public void setContentType(String contentType) {
		this.contentType = contentType; 
	}

	public String getContentType() {
		return contentType;
	}
	
	public void setAnswers(Vector<String> answers) {
		this.answers = answers;
	}

	public Vector<String> getAnswers() {
		return answers;
	}
	
	public void setContentDocument(DominoDocument contentDocument) {
		this.contentDocument = contentDocument;
	}

	public DominoDocument getContentDocument() {
		return contentDocument;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }
}
