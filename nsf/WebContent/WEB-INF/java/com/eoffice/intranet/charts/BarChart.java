package com.eoffice.intranet.charts;

import java.util.Map;

public class BarChart extends Chart {
	private static final long serialVersionUID = 1L;
	
	public BarChart(){
		
	}
	
	public String getBarChartTicks(){
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for (Map.Entry<String, Integer> entry : getData().entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			String identifier = "bar"+i;
			sb.append(identifier+"=["+entry.getValue().toString()+"]\n;");
			i+=1;
		}
		return sb.toString();
	}
	
	public String getBarChartPlot(){
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<getData().size();i++){
			sb.append("bar"+i+",");
		}
		return sb.toString().substring(0, sb.toString().length()-1);
	}
	
	public String getBarChartSeries(){
		//"{label:'" + dataSet3 + "'}" + ((dataIndex3+1<poll.getFieldValue("answers").size())?",":"")}
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, Integer> entry : getData().entrySet()) {
			sb.append("{label:'"+entry.getKey()+"'},");
		}
		
		return sb.toString().substring(0, sb.toString().length()-1);
	}

}
