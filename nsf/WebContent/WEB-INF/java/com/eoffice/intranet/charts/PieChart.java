package com.eoffice.intranet.charts;

import java.util.Map;

public class PieChart extends Chart {
	private static final long serialVersionUID = 1L;
	
	public PieChart(){
		
	}
	
	public String getPieChartData(){
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, Integer> entry : getData().entrySet()) {
			sb.append("['"+entry.getKey()+"',"+entry.getValue().toString()+"],");

		}
		return sb.toString().substring(0, sb.toString().length()-1);
	}

}
