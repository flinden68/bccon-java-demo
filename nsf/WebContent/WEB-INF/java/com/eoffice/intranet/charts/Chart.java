package com.eoffice.intranet.charts;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import lotus.domino.View;

import com.eoffice.basic.configuration.Configuration;
import com.eoffice.utils.JSFUtil;

public class Chart implements Serializable  {
	private static final long serialVersionUID = 1L;
	private HashMap<String,Integer> data = new HashMap<String,Integer>();
	private String pollID;
	public Chart(){
		
	}
	
	public void loadData(String pollID, Vector<String> answers){
		try{setPollID(pollID);
			for(String answer: answers){
				int total = this.getPollAnswerTotal("pollTotalByAnswer", pollID+"##"+answer);				
				getData().put(answer, total);
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
	}
	
	public void updateData(){
		for (Map.Entry<String, Integer> entry : getData().entrySet()) {
			int total = this.getPollAnswerTotal("pollTotalByAnswer", getPollID()+"##"+entry.getKey());
			entry.setValue(total);
		}
	}
	
	public Integer getPollAnswerTotal(String viewName, String key){
		View vw = null;
		try{
			vw = JSFUtil.getCurrentDatabase().getView(viewName);
			vw.refresh();

			if(vw!=null){
				return new Integer(vw.createViewNavFromCategory(key).getCount());
			}
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(vw);
		}
		return new Integer(0);
	}

	public void setPollID(String pollID) {
		this.pollID = pollID;
	}

	public String getPollID() {
		return pollID;
	}

	public void setData(HashMap<String,Integer> data) {
		this.data = data;
	}

	public HashMap<String,Integer> getData() {
		return data;
	}
}
