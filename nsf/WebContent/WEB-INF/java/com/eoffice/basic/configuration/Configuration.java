package com.eoffice.basic.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

import com.eoffice.utils.JSFUtil;
import com.eoffice.utils.OpenLogItem;
import com.ibm.xsp.model.FileRowData;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;
import com.ibm.xsp.model.domino.wrapped.DominoDocument.AttachmentValueHolder;


public class Configuration implements Serializable {
	
	private static final long serialVersionUID = -1374960336981800423L;
	private static final String DEFAULT_OPENLOG_DB_PATH = "admin\\OpenLog.nsf";
	private static final String BEAN_NAME = "configBean";
	private String dateformat = new String("dd-MM-yyyy hh:mm");
	private static OpenLogItem _oli;
	private String openloglocation;
	private String documentId;
	private boolean debug;
	private static DominoDocument configDocument;
	private DominoDocument.AttachmentValueHolder favicon;
	private DominoDocument.AttachmentValueHolder logo; 
	private String oneUIversion;

	/**********************
	 * Constructor        *
	 **********************/
	public Configuration() {		
		try {
			loadConfigurationDocument();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**********************
	 * Methods            *
	 **********************/
	public void determineOneuiVersion(){
		try{
			String version = JSFUtil.getCurrentSession().getNotesVersion();
			if(version.contains("9")){
				this.setOneUIversion("oneuiv3.0.2");
			}else{
				this.setOneUIversion("oneuiv2.1");
			}
			System.out.println("version="+ this.getOneUIversion());
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	public void loadConfigurationDocument(){
	        View vwCfg = null;
	        Document docCfg = null;
	        BufferedReader br = null;
		try {
		        vwCfg = JSFUtil.getCurrentDatabase().getView("configuration"); 
			docCfg = vwCfg.getFirstDocument();
			
			if (null != docCfg) {
				this.setDocumentId(docCfg.getUniversalID());
				DominoDocument dominoDocument = DominoDocument.wrap(docCfg.getParentDatabase().getFilePath(),docCfg,null,null,false,null,null);
				this.setDebug(Boolean.parseBoolean(docCfg.getItemValueString("debug")));
				String logLocation = new String(Configuration.DEFAULT_OPENLOG_DB_PATH);
				if(docCfg.hasItem("openloglocation")){
					logLocation = docCfg.getItemValueString("openloglocation");
				}
				this.setOpenloglocation(logLocation);
				this.setDateformat(docCfg.getItemValueString("dateformat"));
				this.setConfigDocument(dominoDocument);
				loadLogo(dominoDocument);
				loadFavicon(dominoDocument);
				docCfg.recycle();
			}else{
				docCfg = JSFUtil.getCurrentDatabase().createDocument();
				docCfg.replaceItemValue("Form", "configuration");
				docCfg.save(true,true);
				
				this.setDocumentId(docCfg.getUniversalID());
				DominoDocument dominoDocument = DominoDocument.wrap(docCfg.getParentDatabase().getFilePath(),docCfg,null,null,false,null,null);
				this.setConfigDocument(dominoDocument);
				docCfg.recycle();
			}
			JSFUtil.recycleObjects(vwCfg);
			
		} catch (NotesException e) {
			e.printStackTrace();
		} finally{
		    JSFUtil.recycleObjects(docCfg, vwCfg);
		    if(br!=null){
		        try {
                            br.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
		    }
		}
	}
	
	private void loadLogo(DominoDocument dominoDocument){
		try{		
			List<FileRowData> attList = dominoDocument.getAttachmentList("logo");
			if(!attList.isEmpty()){
				setLogo((AttachmentValueHolder) attList.get(0));
			}
		} catch (NotesException e) {
			getOpenLogItem().logError(e);
		}
	}
	
	private void loadFavicon(DominoDocument dominoDocument){
                try{
                        List<FileRowData> attList = dominoDocument.getAttachmentList("favicon");
                        if(!attList.isEmpty()){
                                setFavicon((AttachmentValueHolder) attList.get(0));
                        }
                } catch (Exception e) {
                    Configuration.getOpenLogItem().logError(e);
                }
        }
	
	public static Object getFieldValue(String field){
		try{
			if(JSFUtil.isRecycled(configDocument.getDocument())) { 
				configDocument.restoreWrappedDocument(); 
			}
			if(configDocument.hasItem(field)){
				return configDocument.getValue(field);
			}
			
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}
		return null;
	}
	
	public String getLogoURL(){
		if(logo!=null){
			return logo.getHref();
		}else{
			return "";
		}
	}
	
	public String getFaviconURL(){
            if(favicon!=null){
                    return favicon.getHref();
            }else{
                    return "";
            }
        }
	
	/**********************
	 * Getter and Setter *
	 **********************/
	//access to the configuration bean
	public static Configuration get() {
		return (Configuration) JSFUtil.resolveVariable(BEAN_NAME);
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public boolean isDebug() {
		return debug;
	}

	/*******************
	 * OpenLog methods *
	 *******************/
	public static void setOpenLogItem(OpenLogItem _oli) {
		Configuration._oli = _oli;
	}

	public static OpenLogItem getOpenLogItem() {
		if (_oli == null) {
			_oli = new OpenLogItem();
			String openlogLocation = (String) getFieldValue("openloglocation");
			if (!openlogLocation.equals("")) {
				//_oli.setLogDbName(getLogDbPath());
				openlogLocation = Configuration.DEFAULT_OPENLOG_DB_PATH;	
			}
			_oli.setLogDbName(openlogLocation);
		}
		return _oli;
	}

	public void setConfigDocument(DominoDocument configDocument) {
		Configuration.configDocument = configDocument;
	}

	public DominoDocument getConfigDocument() {
		return configDocument;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}

	public String getDateformat() {
		return dateformat;
	}

	public void setOpenloglocation(String openloglocation) {
		this.openloglocation = openloglocation;
	}

	public String getOpenloglocation() {
		return openloglocation;
	}

	public void setOneUIversion(String oneUIversion) {
		this.oneUIversion = oneUIversion;
	}

	public String getOneUIversion() {
		return oneUIversion;
	}

	public void setLogo(DominoDocument.AttachmentValueHolder logo) {
		this.logo = logo;
	}

	public DominoDocument.AttachmentValueHolder getLogo() {
		return logo;
	}
	
	public void setFavicon(DominoDocument.AttachmentValueHolder favicon) {
            this.favicon = favicon;
        }
    
        public DominoDocument.AttachmentValueHolder getFavicon() {
                return favicon;
        }

}
