package com.eoffice.basic.configuration;

import java.util.ArrayList;
import java.util.Vector;

import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;

import com.eoffice.utils.JSFUtil;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class RichTextToolbar {
	private String documentId;
	private static DominoDocument configDocument;
	private ArrayList<String> fieldNames;
	public RichTextToolbar(){
		loadFieldNames();
		loadConfigurationDocument();
		
	}
	
	/**********************
	 * Methods            *
	 **********************/
	public void loadFieldNames(){
		fieldNames = new ArrayList<String>();
		fieldNames.add("barExtra");
		fieldNames.add("barSource");
		fieldNames.add("barFont");
		fieldNames.add("barColor");
		fieldNames.add("barText");
		fieldNames.add("barTextExtended");
		fieldNames.add("barList");
		fieldNames.add("barPosition");
		fieldNames.add("barLink");
		fieldNames.add("barExtended");
	}
	public void loadConfigurationDocument(){
		try {
			View vwCfg = JSFUtil.getCurrentDatabase().getView("toolbar"); 
			Document docCfg = vwCfg.getFirstDocument();
			if (null != docCfg) {
				this.setDocumentId(docCfg.getUniversalID());
				DominoDocument dominoDocument = DominoDocument.wrap(docCfg.getParentDatabase().getFilePath(),docCfg,null,null,false,null,null);
				RichTextToolbar.setConfigDocument(dominoDocument);
				docCfg.recycle();
			}			
			JSFUtil.recycleObjects(vwCfg);
			
		} catch (NotesException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public String composeToolbar(){
		StringBuffer sb = new StringBuffer();
		try{
			if(JSFUtil.isRecycled(configDocument.getDocument())) { 
				configDocument.restoreWrappedDocument(); 
			}
			int j = 1;
			sb.append("[");
			for(String field: fieldNames){
				if(this.isVector(field)){
					Vector<String> values = (Vector<String>)this.getFieldValue(field);
					int i = 1;
					sb.append("[");
					for(String value:values){
						sb.append("'"+value+"'");
						if(i<values.size()){
							sb.append(",");
						}
						i+=1;
					}
					sb.append("]");
					if(j<fieldNames.size()){
						sb.append(",");
					}
				}else{
					String singleValue = (String)this.getFieldValue(field);
					if(singleValue!=null){
						sb.append("['"+singleValue+"']");
						if(j<fieldNames.size()){
							sb.append(",");
						}
					}
				}				
				j+=1;
			}
			sb.append("]");
			return sb.toString();
			
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}
		return null;
	}
	
	public Object getFieldValue(String field){
		try{
			if(JSFUtil.isRecycled(configDocument.getDocument())) { 
				configDocument.restoreWrappedDocument(); 
			}
			if(configDocument.hasItem(field)){
				return configDocument.getValue(field);
			}
			
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}
		return null;
	}
	
	public boolean isVector(String field){
		try{
			if(JSFUtil.isRecycled(configDocument.getDocument())) { 
				configDocument.restoreWrappedDocument(); 
			}
			if(configDocument.hasItem(field)){
				Object value = configDocument.getValue(field);
				if(value!=null){
					if(value.getClass().getName().equals("java.util.Vector")){
						return true;
					}
				}
			}
			
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}
		return false;
	}
	
	/**********************
	 * Getter and Setter *
	 **********************/
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public static void setConfigDocument(DominoDocument configDocument) {
		RichTextToolbar.configDocument = configDocument;
	}

	public static DominoDocument getConfigDocument() {
		return configDocument;
	}
}
