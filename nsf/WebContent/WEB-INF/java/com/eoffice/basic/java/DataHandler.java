package com.eoffice.basic.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import lotus.domino.Database;
import lotus.domino.DateTime;
import lotus.domino.Document;
import lotus.domino.View;

import com.eoffice.basic.configuration.Configuration;
import com.eoffice.utils.JSFUtil;
import com.eoffice.utils.OpenLogItem;
import com.ibm.commons.util.io.json.JsonJavaObject;


public class DataHandler implements Serializable  {
	private static final long serialVersionUID = 1L;
	private static OpenLogItem _oli;
	private static String LogDbPath = "admin\\OpenLog.nsf";
	
	public DataHandler(){
		
	}
	
	public String runUpdate(String sUNID){
		View vw = null;
		Document doc = null;
		StringBuffer sb = new StringBuffer();

		try{	
			Database db = JSFUtil.getCurrentDatabase();
			vw = db.getView("matchByUNID");
			doc = vw.getDocumentByKey(sUNID);
			
			if(doc!=null){
				String msg = doc.getItemValueString("Message");
	
				sb.append("event: message\n");
				sb.append("data: {\"msg\": \"" + msg + "\"}\n\n");
				sb.append("event: teams\n");
				sb.append("data: {\"home\": \""+doc.getItemValueString("HomeTeam")+"\", \"guest\":\""+doc.getItemValueString("GuestTeam")+"\"}\n\n");
				
				sb.append("event: scores\n");
				sb.append("data: {\"home\": \""+doc.getItemValueInteger("HomeGoals")+"\", \"guest\":\""+doc.getItemValueInteger("GuestGoals")+"\"}\n\n");
							
				sb.append("event: cards\n");
				sb.append("data: {\"home\": {\"red\":\""+doc.getItemValueInteger("HomeRedCards")+"\",\"yellow\":\""+doc.getItemValueInteger("HomeYellowCards")+"\"}, \"guest\":{\"red\":\""+doc.getItemValueInteger("GuestRedCards")+"\",\"yellow\":\""+doc.getItemValueInteger("GuestYellowCards")+"\"}}\n\n");
	
				sb.append("event: error1\n");
				sb.append("data: {\"type\": \"no ERROR\"}\n\n");
				sb.append("retry: 1000\n");

			}
		}catch(Exception e){
			getOpenLogItem().logError(e);
		}
		finally{
			JSFUtil.recycleObjects(vw,doc);
		}
		return sb.toString();
	}
	
	//JSON Example
	public String getPolls(){
		View vwWidgets = null;
		Document docPage = null;
		Document docNext = null;
		Date dateStart = null;
		Date dateEnd = null;
		boolean addPoll = false;
		
		JsonJavaObject json = new JsonJavaObject();
		ArrayList<JsonJavaObject> list = new ArrayList<JsonJavaObject>();
		
		//String jsonValue = null;
		try{
			//json.putJavaDate("timestamp", new Date()); 
			vwWidgets = JSFUtil.getCurrentDatabase().getView("pollsForWidget");
			if(vwWidgets!=null){
			docPage = vwWidgets.getFirstDocument();
			
				while(docPage!=null){
					docNext = vwWidgets.getNextDocument(docPage);
					addPoll = false;
					if(docPage.hasItem("startdate")){
						dateStart = ((DateTime)docPage.getItemValueDateTimeArray("startdate").elementAt(0)).toJavaDate();
					}
					
					if(docPage.hasItem("enddateShow")){
						dateEnd = ((DateTime)docPage.getItemValueDateTimeArray("enddateShow").elementAt(0)).toJavaDate();
					}
					
					if(dateEnd!=null&&dateStart!=null){
						addPoll = JSFUtil.isTodayInBetween(dateStart, dateEnd);
					}
					
					if(addPoll){
						
						JsonJavaObject jsonPoll = new JsonJavaObject();
						jsonPoll.putString("pollid", Integer.toString(docPage.getItemValueInteger("pollid")));
						jsonPoll.putString("question", docPage.getItemValueString("question"));
						
						list.add(jsonPoll);
					}
					docPage.recycle();
					docPage = docNext;
				}
			}
			
			json.putObject("polls", list);
			
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			JSFUtil.recycleObjects(docPage, docNext, vwWidgets);
		}

		return json.toString();
	}
	
	public void finallyTest(String sUNID){
		View vw = null;
		Document doc = null;
		InputStream inputStream = null;
		File file = null;
		try{	
			Database db = JSFUtil.getCurrentDatabase();
			vw = db.getView("matchByUNID");
			doc = vw.getDocumentByKey(sUNID);
			
			if(doc!=null){
				String fileName = System.getProperty("java.io.tmpdir")+File.separator+sUNID+".xml";
				//create file of xml data
				file = new File(fileName);
				// write the inputStream to a FileOutputStream
				inputStream = new FileInputStream(file);
				@SuppressWarnings("unused")
                                int read = 0;
				byte[] bytes = new byte[1024];
		 
				while ((read = inputStream.read(bytes)) != -1) {
					System.out.println(bytes);
				}
			}
		}catch(Exception e){
			getOpenLogItem().logError(e);
		}
		finally{
			JSFUtil.recycleObjects(vw,doc);
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(file != null) {
				file.delete();
			}
		}
	}
	
	
	/*OpenLog methods*/
	static OpenLogItem getOpenLogItem() {
		if (_oli == null) {
			_oli = new OpenLogItem();
			if (!getLogDbPath().equals("")) {
				_oli.setLogDbName(getLogDbPath());
			}
		}
		return _oli;
	}
	
	public static String getLogDbPath() {
		return LogDbPath;
	}

	public static void setLogDbPath(String logDbPath) {
		LogDbPath = logDbPath;
	}
}
