package com.eoffice.utils;
//Copyright 2011 Paul Withers, Nathan T. Freeman & Tim Tripcony
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.faces.application.Application; 
import javax.faces.el.ValueBinding; 
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import java.lang.reflect.Method;
import lotus.domino.Base;
import lotus.domino.local.NotesBase;

import com.eoffice.basic.configuration.Configuration;
import com.ibm.xsp.application.DesignerApplicationEx;
import com.ibm.xsp.component.UIViewRootEx;
import com.ibm.xsp.designer.context.XSPContext;
import com.ibm.xsp.designer.context.XSPUrl;
//import com.ibm.domino.xsp.bridge.http.util.SingleValueEnumeration;
import com.ibm.xsp.designer.context.XSPUserAgent;

public class JSFUtil {

	//private static OpenLogItem oli = new OpenLogItem();

	public static DesignerApplicationEx getApplication() {
		return (DesignerApplicationEx) getFacesContext().getApplication();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getApplicationScope() {
		return getServletContext().getApplicationMap();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getCompositeData() {
		return (Map<String, Object>) getVariableResolver().resolveVariable(getFacesContext(), "compositeData");
	}

	public static Database getCurrentDatabase() {
		return (Database) getVariableResolver().resolveVariable(getFacesContext(), "database");
	}

	public static Session getCurrentSession() {
		return (Session) getVariableResolver().resolveVariable(getFacesContext(), "session");
	}

	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getRequestScope() {
		return getServletContext().getRequestMap();
	}

	public static ExternalContext getServletContext() {
		return getFacesContext().getExternalContext();
	}
	
	public static Object getRequest() {
		return getFacesContext().getExternalContext().getRequest();
	}
	
	public static Object getResponse() {
		return getFacesContext().getExternalContext().getResponse();
	}
 
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getSessionScope() {
		return getServletContext().getSessionMap();
	}

	private static VariableResolver getVariableResolver() {
		return getApplication().getVariableResolver();
	}

	public static UIViewRootEx getViewRoot() {
		return (UIViewRootEx) getFacesContext().getViewRoot();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getViewScope() {
		return getViewRoot().getViewMap();
	}

	public static XSPContext getXSPContext() {
		return XSPContext.getXSPContext(getFacesContext());
	}

	public static Object resolveVariable(String variable) {
		return FacesContext.getCurrentInstance().getApplication().getVariableResolver().resolveVariable(FacesContext.getCurrentInstance(), variable);
	}
	
	public static XSPUserAgent getUserAgent(){
		return getXSPContext().getUserAgent();
	}
	
	public static String getRequestURI(){
	    return ((HttpServletRequest) getRequest()).getRequestURI();
	}

	public static String getQueryStringParameter(String q){
		try{System.out.println(XSPContext.getXSPContext(getFacesContext()).getUrlParameter(q));
			return XSPContext.getXSPContext(getFacesContext()).getUrlParameter(q);
			
		}catch(Exception e){
			return "";
		}
	}
	
	public String getPageName(){
	    FacesContext fc = getFacesContext();
	    System.out.println(fc.getExternalContext().getRequestContextPath());
	    return fc.getExternalContext().getRequestContextPath();
	    
	    //facesContext.getExternalContext().getRequest().getRequestURI();
	    //return ((Object) getRequest()).getRequestURI();
	}
	
	@SuppressWarnings("unchecked")
	public static String getProfileUNID(String profileKey) {
		try {
			Vector profHash = getCurrentSession().evaluate("@Text(@Password(\"" + profileKey + "\"))");
			String tmpUNID = profHash.firstElement().toString();
			tmpUNID = tmpUNID.substring(1, tmpUNID.length() - 1);
			return tmpUNID;
		} catch (NotesException e) {
			//oli.logError(e);
			return "";
		}
	}
	
	public static void showMessageOnconsole(String smessage){
		System.out.println(smessage);
	}
	
	public static Object getBean(String expr){  
	    FacesContext context = FacesContext.getCurrentInstance();  
	    Application app = context.getApplication();  
	    ValueBinding binding = app.createValueBinding("#{" + expr + "}");  
	    Object value = binding.getValue(context);  
	    return value;  
	}
	/** 
     * The method creates a {@link javax.faces.el.ValueBinding} from the 
     * specified value binding expression and returns its current value.<br> 
     * <br> 
     * If the expression references a managed bean or its properties and the bean has not 
     * been created yet, it gets created by the JSF runtime. 
     * 
     * @param ref value binding expression, e.g. #{Bean1.property} 
     * @return value of ValueBinding 
     * throws javax.faces.el.ReferenceSyntaxException if the specified <code>ref</code> has invalid syntax 
     */ 
    public static Object getBindingValue(String ref) { 
            FacesContext context=FacesContext.getCurrentInstance(); 
            Application application=context.getApplication(); 
            return application.createValueBinding(ref).getValue(context); 
    } 

   /** 
     * The method creates a {@link javax.faces.el.ValueBinding} from the 
     * specified value binding expression and sets a new value for it.<br> 
     * <br> 
     * If the expression references a managed bean and the bean has not 
     * been created yet, it gets created by the JSF runtime. 
     * 
     * @param ref value binding expression, e.g. #{Bean1.property} 
     * @param newObject new value for the ValueBinding 
     * throws javax.faces.el.ReferenceSyntaxException if the specified <code>ref</code> has invalid syntax 
     */ 
    public static void setBindingValue(String ref, Object newObject) { 
            FacesContext context=FacesContext.getCurrentInstance(); 
            Application application=context.getApplication(); 
            ValueBinding binding=application.createValueBinding(ref); 
            binding.setValue(context, newObject); 
    } 

   /** 
     * The method returns the value of a global JavaScript variable. 
     * 
     * @param varName variable name 
     * @return value 
     * @throws javax.faces.el.EvaluationException if an exception is thrown while resolving the variable name 
     */ 
    public static Object getVariableValue(String varName) { 
            FacesContext context = FacesContext.getCurrentInstance(); 
            return context.getApplication().getVariableResolver().resolveVariable(context, varName); 
    } 

   /** 
     * Finds an UIComponent by its component identifier in the current 
     * component tree. 
     * 
     * @param compId the component identifier to search for 
     * @return found UIComponent or null 
     * 
     * @throws NullPointerException if <code>compId</code> is null 
     */ 
    public static UIComponent findComponent(String compId) { 
            return findComponent(FacesContext.getCurrentInstance().getViewRoot(), compId); 
    } 

   /** 
     * Finds an UIComponent by its component identifier in the component tree 
     * below the specified <code>topComponent</code> top component. 
     * 
     * @param topComponent first component to be checked 
     * @param compId the component identifier to search for 
     * @return found UIComponent or null 
     * 
     * @throws NullPointerException if <code>compId</code> is null 
     */ 
    @SuppressWarnings("unchecked")
	public static UIComponent findComponent(UIComponent topComponent, String compId) { 
            if (compId==null) 
                    throw new NullPointerException("Component identifier cannot be null"); 

            if (compId.equals(topComponent.getId())) 
                    return topComponent; 

            if (topComponent.getChildCount()>0) { 
                    List<UIComponent> childComponents= topComponent.getChildren(); 

                    for (UIComponent currChildComponent : childComponents) { 
                            UIComponent foundComponent=findComponent(currChildComponent, compId); 
                            if (foundComponent!=null) 
                                    return foundComponent; 
                    } 
            } 
            return null; 
    }
    
    @SuppressWarnings("unchecked")
	public static Object getSubmittedValueCheckbox(String compId){
    	Object o = JSFUtil.getSubmittedValue(compId);
    	if(o.getClass().getName()=="java.util.ArrayList"){
			return ((ArrayList) o).get(0);
		}else{
			//Vector
			return o;
		}
    }
    
    public static Object getSubmittedValue(String compId) {
    	UIComponent c = findComponent(FacesContext.getCurrentInstance().getViewRoot(), compId);
    	// value submitted from the browser 
    	Object o = ((UIInput) c).getSubmittedValue(); 
    	
    	if( null == o ){ 
           // else not yet submitted
           o = ((UIInput) c).getValue();
    	}
    	return o;
    }
    
    public static String getResource(String bundleVar, String name) {
    	 XSPContext xspContext = XSPContext.getXSPContext(FacesContext
    	   .getCurrentInstance());
    	 
    	 try {
    	  // There has to be bundle defined with var name <bundlevar>
    	  ResourceBundle bundle = xspContext.bundle(bundleVar);
    	  if (bundle != null) {
    	   return bundle.getString(name);
    	  }
    	 } catch (IOException e) {
    	  e.printStackTrace();
    	 }
    	 
    	 return "";
    	}
    
    /*
     * function getAttachmentURL(docID:java.lang.String, attachmentName:java.lang.String) {
    var base = getBaseURL();
    var middle = "/xsp/.ibmmodres/domino/OpenAttachment";
    if (base.substr(0,4) == "/xsp") {
        middle += base.substr(4);
    } else {
        middle += base;
    }
    var result = base + middle + "/" + docID + "/$File/" + attachmentName + "?Open";
    return result;
}
     */
    public static String getAttachmentURL(String docID, String attachmentName){
    	try {
    		String base = getBaseURL();
    		return base + "/0/" + docID + "/$File/" + attachmentName;
    	} catch (Exception e) {
       	  	e.printStackTrace();
       	 }
    	 return "";
    }
    
    public static String getAttachmentOpenURL(String docID, String attachmentName){
    	try {
    		String base = getBaseURL();
    		String middle = "/xsp/.ibmmodres/domino/OpenAttachment";
    		if(base.substring(0,4)=="/xsp"){
    			middle += base.substring(4);
    		}else{
    			middle += base;
    		}
    		return base + middle + "/" + docID + "/$File/" + attachmentName + "?Open";
    	} catch (Exception e) {
       	  	e.printStackTrace();
       	 }
    	 return "";
    }
    /*
     * http://www.wissel.net/blog/d6plinks/SHWL-86QKNM
     * 
     function getBaseURL() {
    var curURL = context.getUrl();
    var curAdr = curURL.getAddress();
    var rel = curURL.getSiteRelativeAddress(context);
    var step1 = curAdr.substr(0,curAdr.indexOf(rel));
   
    // Now cut off the http
    var step2 = step1.substr(step1.indexOf("//")+2);
    var result = step2.substr(step2.indexOf("/"));
    return result;
   
   }
     */
    
    public static String getBaseURL(){
    	 try {
	    	XSPUrl url = JSFUtil.getXSPContext().getUrl();
	    	String address = url.getAddress();
	    	String rel = url.getSiteRelativeAddress(JSFUtil.getXSPContext());
	    	
	    	String step1 = address.substring(0,address.indexOf(rel));
	    	String step2 = step1.substring(step1.indexOf("//")+2);
	    	
	    	return step2.substring(step2.indexOf("/"));
    	 } catch (Exception e) {
       	  	e.printStackTrace();
       	 }
    	 return "";
    }
    
    public static boolean isRecycled( Base object ) {
        boolean isRecycled = true;
        if( ! ( object instanceof NotesBase ) ) { 
            // No reason to test non-NotesBase objects -> isRecycled = true
            return isRecycled;
        }

        try {
            NotesBase notesObject = (NotesBase) object;
            Method isDead = notesObject.getClass().getSuperclass().getDeclaredMethod( "isDead" );
            isDead.setAccessible( true );       

            isRecycled = (Boolean) isDead.invoke( notesObject );
        } catch ( Throwable exception ) {
            // Exception handling
        }

        return isRecycled;
    }
    
    public static String getLTPAToken(){
    	if(getFacesContext().getExternalContext().getRequestCookieMap().containsKey("LtpaToken")){
    		javax.servlet.http.Cookie token = (Cookie) getFacesContext().getExternalContext().getRequestCookieMap().get("LtpaToken");
    		
    		//return token.getValue().substring(0,token.getValue().indexOf("/"));
    		return token.getValue();
    	}else{
    		return null;
    	}
    }
    
    /***************************
	 * Generic Recycle function
	 ***************************/
	public static void recycleObjects(final Object... dominoObjects) {
	    for (Object dominoObject : dominoObjects) {
	        if (null != dominoObject) {
	            if (dominoObject instanceof Base) {
	                try {
	                    ((Base)dominoObject).recycle();
	                } catch (NotesException recycleSucks) {
	                    // optionally log exception
	                }
	            }
	        }
	    }
	}
	
	/***************************
	 * Generic Date Methods
	 ***************************/
	public static boolean isTodayInBetween(Date dateStart, Date dateEnd){
		try{
			Date dateCheck = new Date();
			return dateCheck.after(dateStart)&&dateCheck.before(dateEnd);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;
	}
	
	public static boolean isDateInBetween(Date dateCheck, Date dateStart, Date dateEnd){
		try{
			return dateCheck.after(dateStart)&&dateCheck.before(dateEnd);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;
	}
	
	public static boolean isDateBeforeToday(Date dateCheck){
		try{
			Date dateCompare = new Date();
			return dateCompare.before(dateCheck);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;
	}
	
	public static boolean isDateBefore(Date dateCheck, Date dateCompare){
		try{
			return dateCompare.before(dateCheck);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;
	}
	
	public static boolean isDateAfterToday(Date dateCheck){
		try{
			Date dateCompare = new Date();
			return dateCompare.after(dateCheck);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;
	}
	
	public static boolean isDateAfter(Date dateCheck, Date dateCompare){
		try{
			return dateCompare.after(dateCheck);
		}catch(Exception e){
			Configuration.getOpenLogItem().logError(e);
		}finally{
			
		}
		return false;
	}
}